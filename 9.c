/*Write a C programme to complete the following tasks.

Create a file named "assignment9.txt" and open it in write mode. Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.

Open the file in read mode and read the text from the file and print it to the output screen.

Open the file in append mode and append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.*/

#include <stdio.h>
#include <string.h>

int main()
{
    FILE *fp;
    char a[100];

//writing to the file
    
    fp = fopen("assignment9.txt", "w");
    fputs("UCSC is one of the leading institutes in sri lanka for computing studies.", fp);
    printf("data wrote successfully\n"); 
    fclose(fp);

//reading from file

    fp = fopen("assignment9.txt", "r");

    if (fp == NULL)
    {
        printf("Couldn't locate the file\n");
    }
    else
    {
        while (!feof(fp))
        {
            fgets(a, 100, fp);
            puts(a);
        }
    }

    fclose(fp);

//appending to the file

    fp = fopen("assignment9.txt", "a");
    fputs("UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.", fp);
    printf("data appended successfully\n");
    fclose(fp);

    return 0;
}
